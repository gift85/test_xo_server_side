<?php
return [
    'db' => [
        'name' => 'db_name',
        'user' => 'user',
        'pass' => 'password',
        'host' => 'localhost',
        'driver' => 'mysql'
    ],
	'base' => [
		'path' => __DIR__ . DIRECTORY_SEPARATOR,
	]
];
