<?php

namespace App;

class App
{
    private $settings = [];
    private $board = [];
    private $get;
    private $post;
    private $id;
    private $owner;


    public function __construct(array $array)
    {
        $this->settings = $array['settings'];
        $this->board = ['', '', '', '', '', '', '', '', ''];
        $this->db = new Sql($this->settings['db']);
        $this->get = $_GET;
        $this->post = $_POST;
    }

    public function request_handler()
    {
        var_dump($this->get);
        var_dump($this->post);

        if(empty($this->get)){
            $this->owner = $this->get_owner();
            $this->new_game();
            if($this->owner == 'x'){
                $this->game_step();
            }
        } elseif(!empty($this->get['id'])){
            echo 'id not empty!<br>';
            $this->id = $this->get['id'];
            $this->get_game();
        }

    }

    private function get_owner()
    {
        if(empty($this->id)){
            $whos_first = mt_rand(0, 100);
            //x - computer
            $this->owner = $whos_first >= 50 ? 'x' : 'o';
        }
        $owner = 'x';
        return $owner;
    }

    public function new_game()
    {
        //$id = $this->db->get_new_id($this->board, $this->owner);
        $this->id = 20;
    }

    public function get_game()
    {
        echo "searching game..$this->id <br>";
        $board = $this->db->get_board($this->id);
        var_dump($board);
    }

    public function game_step()
    {
        echo "doing step <br>";
        $cross_board = ['', '', '', '', '', '', '', '', ''];
        var_dump($cross_board);
        $this->board[8] = $this->owner;
        $this->board[6] = $this->owner;
        $this->board[2] = $this->owner;
        $free_cells = array_intersect_assoc($cross_board, $this->board);
        var_dump($free_cells);
        $choice = array_rand($free_cells);
        var_dump($choice);
        $this->board[$choice] = $this->owner;
        var_dump($this->board);
    }

    private function check_lines($mark)
    {

    }

    public function run()
    {
        $this->request_handler();
        var_dump($this->board);
    }
}