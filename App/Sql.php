<?php

namespace App;

use PDO, PDOException;

class Sql
{
    protected $db;
    private $table = 'tables';

    public function __construct(array $dbsettings)
    {
        $host = $dbsettings['host'];
        $dbname = $dbsettings['name'];
        $user = $dbsettings['user'];
        $pass = $dbsettings['pass'];
        $driver = $dbsettings['driver'];
        try{
            $this->db = new PDO("$driver:host=$host;dbname=$dbname", $user, $pass, [
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'
            ]);
        }
        catch(PDOException $e){
            throw new \Exception("DB CONNECTION ERROR: {$e->getMessage()}");
        }
        //echo 'db connected';
    }

    public function get_new_id($board, $owner)
    {
        $sql = "INSERT INTO $this->table (board, last_step, step_owner) VALUES (:board, :last_step, :step_owner)";
        $params['last_step'] = time() + 60;
        $params['board'] = json_encode($board);
        $params['step_owner'] = $owner;

        $this->execute_query($sql, $params);
        return $this->db->lastInsertId();
    }

    public function get_board($id)
    {
        $sql = "SELECT board FROM $this->table WHERE id_table = :id";
        $query = $this->execute_query($sql, ['id' => $id]);
        $res = $query->fetch();
        return json_decode($res['board']);
    }

    public function select(string $sql, array $params = []):array{
        $query = $this->execute_query($sql, $params);
        return $query->fetchAll();
    }

    public function insert(string $table, array $params):int{
        $keys = array_keys($params);
        $masks = array_map(function (string $key){
            return ":$key";
        }, $keys);

        $fields = implode(', ', $keys);
        $values = implode(', ', $masks);

        $sql = "INSERT INTO $this->table ($fields) VALUES ($values)";
        $query = $this->execute_query($sql, $params);
        return $query->rowCount();
    }

    public function update(string $table, array $set, string $where, array $params = []):int{
        $keys = array_keys($set);
        $masks = array_map(function ($key){
            return "$key = :$key";
        }, $keys);
        $pairs = implode(', ', $masks);

        $merged_params = array_merge($set, $params);
        $sql = "UPDATE $this->table SET $pairs WHERE $where";
        $query = $this->execute_query($sql, $merged_params);
        return $query->rowCount();
    }

    public function delete(string $table, string $where, array $params = []):int{
        $sql = "DELETE FROM $this->table WHERE $where";
        $query = $this->execute_query($sql, $params);
        return $query->rowCount();
    }


    protected function execute_query(string $sql, array $params):\PDOStatement{
        $query = $this->db->prepare($sql);
        try{
            $query->execute($params);
        }
        catch(PDOException $e){
            throw new \Exception("ERROR {$e->getMessage()}");
        }
        return $query;
    }
}
